#ΥπολογισμόςΔιακρίνουσαςΡιζών
#v11-06032019

import cmath

def dcalc(A, B, C):

    global diakr

    diakr = int((B*B) - (4*A*C))
    print('\n∙ Διακρίνουσα: '+str(diakr))
    rcalc(diakr)

def rcalc(diakr):

    if diakr > 0:
        riza1 = ((-B) + cmath.sqrt(diakr)) / (2*A)
        riza2 = ((-B) - cmath.sqrt(diakr)) / (2*A)
        print('∙ X¹: '+str(riza1)+'\n∙ Χ²: '+str(riza2)+'\n')
        start()
    elif diakr == 0:
        riza0 = (-B) / (2*A)
        print('∙ X⁰: '+str(riza0)+'\n')
        start()
    else:
        print('∙ X∉R\n')
        start()

def error(x):

    if x == 1:
        print('\nΣφάλμα! Ο συντελεστής Α πρέπει να ειναι <> 0 για να αποτελεί η παράσταση τριώνυμο\n')
        start()
    elif x == 2:
        print('\nΣφάλμα! Οι συντελεστές πρέπει να είναι πραγματικοί αριθμοί\n')
        start()

def start():

    global A
    global B
    global C

    print('◊ Τριώνυμο Μορφής Αx^2 + Βx + Γ = 0')
    try:
        A = int(input('Α > '))
        if A == 0:
            error(1)
        B = int(input('Β > '))
        C = int(input('Γ > '))
    except ValueError:
        error(2)
    dcalc(A, B, C)

start()
